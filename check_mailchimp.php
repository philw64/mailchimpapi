<?php get_header(); ?>
<?php
    /* This page is only available to admin users
     *
     */

    if( current_user_can('administrator') ) {
        //if (isset($_GET['test'])) {
            //test_subscribe();
        //}

        $page_body =  '<h2>Mailchip Health Check</h2>';
        $api = MailchimpAPI::Instance();
        $response = $api->ping();
        if (!$response) {
            $page_body .= 'Error: '.var_export($api->get_last_error(),true);
            $page_body .= '<br><p>Request: '.var_export($api->get_last_request(),true).'</p>';
            $page_body .= '<br><p>Response: '.var_export($api->get_last_response(),true).'</p>';
            $page_body .= '<br><p>Error: '.var_export($api->get_last_error(),true).'</p>';
        } else {
            $page_body .= '<p>Mailchimp reports: '.$response['health_status'].'</p>';
        }

        $page_body .=  '<h2>Checking for Mailchip Lists</h2>';
        $response = $api->lists();
        if (!$response) {
            $page_body .= 'Error: '.var_export($api->get_last_error(),true);
            $page_body .= '<br><p>Request: '.var_export($api->get_last_request(),true).'</p>';
            $page_body .= '<br><p>Response: '.var_export($api->get_last_response(),true).'</p>';
            $page_body .= '<br><p>Error: '.var_export($api->get_last_error(),true).'</p>';
        } else {
            $page_body .= '<h3>Available lists:</h3>';
            foreach ($response['lists'] as $key=>$list) {
                $page_body .= "<p>ID: ".$list['id'].', Name: '.$list['name'].'</p>';
            }
        }
        echo '<section class="s25"></section><section class="s22"><div class="inner">'.$page_body.'</div></section>';

    } else {
        // Non admin users get redirected to the home page.
        wp_redirect(home_url());
    }
/*
    function test_subscribe() {
        $page_body =  '<h2>Mailchip Subscribe Test</h2>';
        $api = MailchimpAPI::Instance();
        $response = MailchimpAPI::Instance()->subscribe_user('22d131dd84','Test', 'Subscriber1', 'artistb277@gmail.com');
        if (!$response) {
            $page_body .= 'Error: '.var_export($api->get_last_error(),true);
            $page_body .= '<br><p>Request: '.var_export($api->get_last_request(),true).'</p>';
            $page_body .= '<br><p>Response: '.var_export($api->get_last_response(),true).'</p>';
            $page_body .= '<br><p>Error: '.var_export($api->get_last_error(),true).'</p>';
        } else {
            $page_body .= '<p>Mailchimp reports: '.$response['health_status'].'</p>';
        }
        echo '<section class="s25"></section><section class="s22"><div class="inner">'.$page_body.'</div></section>';
    }
*/
?>
<?php get_footer(); ?>