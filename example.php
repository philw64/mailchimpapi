<?php

if ( !class_exists( 'MAILCHIMP_TEST' ) ) {
    class MAILCHIMP_TEST {

        const TAG = "Subscribed to Whatever";

        /**
         * This code makes the class a singleton, only instantiated once when
         * MAILCHIMP_TEST::Instance(). is invoked. Calling Instance again will
         * return the same instance pointer.
         */
        public static function Instance()
        {
            static $inst = null;
            if ($inst === null) {
                $inst = new MAILCHIMP_TEST();
            }
            return $inst;
        }

        private function __construct()
        {
            add_action ( 'pms_member_add_subscription', array($this,'new_member_subscribed' ),10,6);

        }

        /**
         *
         * @param type $insert_result
         * @param type $user_id
         * @param type $subscription_plan_id
         * @param type $start_date
         * @param type $expiration_date
         * @param type $status
         * @return type
         */
        function new_member_subscribed( $insert_result, $user_id, $subscription_plan_id, $start_date, $expiration_date, $status ) {
            // Loop through Mailchimp accounts to which member should be subscribed
            if( have_rows('mailchimp_mailing_lists','options') ) {
                while ( have_rows('mailchimp_mailing_lists','options') ) {
                    the_row();
                    $list_id = get_sub_field('list_id');

                    $result = MailchimpAPI::Instance()->subscribe_user(
                        $list_id,
                        $user->first_name,
                        $user->last_name,
                        $user->user_email,
                        TAG);

            }
        }
    }
}

