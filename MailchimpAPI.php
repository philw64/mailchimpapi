<?php
/*
 *
 */
if ( !class_exists( 'MailchimpAPI' ) ) {
    class MailchimpAPI {

        private $errors;
        private $apiKey;
        private $baseUrl;
        private $timeout;
        private $last_request;
        private $last_response;
        private $success;

        const VERSION  = '3.0/';

        const GET = 'get';
        const PUT = 'put';
        const POST = 'post';
        const PING = 'ping/';
        const LISTS = 'lists/';
        const SUBSCRIBE = "lists/%s/members";

        /**
         * This code makes the class a singleton, only instantiated once when
         * MailchimpAPI::Instance(). is invoked. Calling Instance again will
         * return the same instance pointer.
         */
        public static function Instance()
        {
            static $inst = null;
            if ($inst === null) {
                $inst = new MailchimpAPI();
            }
            return $inst;
        }

        public function __construct() {
            $this->apiKey = get_field('mailchimp_api_key','options');

            $this->baseUrl = get_field('mailchimp_base_url','options');
            $this->timeout = 30;
        }

        private function set_headers(&$curlHandle, $command) {
            $httpHeader = array(
                'Accept: application/vnd.api+json',
                'Content-Type: application/vnd.api+json',
                'Authorization: apikey ' . $this->apiKey
            );


            if ($command === MailchimpAPI::PUT) {
                $httpHeader[] = 'Allow: PUT, PATCH, POST';
            }

            $options = [
                CURLOPT_HTTPHEADER => $httpHeader,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_VERBOSE => true,
                CURLOPT_HEADER => true,
                CURLOPT_TIMEOUT => $this->timeout,
                CURLOPT_SSL_VERIFYPEER => true,
                CURLOPT_ENCODING => '',
                CURLINFO_HEADER_OUT => true,
            ];

            curl_setopt_array($curlHandle,$options);
        }

          private function initialize_response($command, $path, $url) {

            $this->errors = '';

            $this->success = false;

            $this->last_response = array(
                'headers'     => null, // array of details from curl_getinfo()
                'httpHeaders' => null, // array of HTTP headers
                'body'        => null // content of the response
            );

            $this->last_request = array(
                'method'  => $command,
                'path'    => $path,
                'url'     => $url,
                'body'    => '',
                'timeout' => $this->timeout,
            );

            return $this->last_response;
        }
        /**
         * Get the HTTP headers as an array of header-name => header-value pairs.
         *
         * The "Link" header is parsed into an associative array based on the
         * rel names it contains. The original value is available under
         * the "_raw" key.
         *
         * @param string $headersAsString
         *
         * @return array
         */
        private function convert_headers_to_array($headersAsString)
        {
            $headers = array();

            foreach (explode("\r\n", $headersAsString) as $i => $line) {
                if (preg_match('/HTTP\/[1-2]/', substr($line, 0, 7)) === 1) { // http code
                    continue;
                }

                $line = trim($line);
                if (empty($line)) {
                    continue;
                }

                list($key, $value) = explode(': ', $line);

                if ($key == 'Link') {
                    $value = array_merge(
                        array('_raw' => $value),
                        $this->get_link_header($value)
                    );
                }

                $headers[$key] = $value;
            }

            return $headers;
        }

        /**
         * Extract all rel => URL pairs from the provided Link header value
         *
         * Mailchimp only implements the URI reference and relation type from
         * RFC 5988, so the value of the header is something like this:
         *
         * 'https://us13.api.mailchimp.com/schema/3.0/Lists/Instance.json; rel="describedBy",
         * <https://us13.admin.mailchimp.com/lists/members/?id=XXXX>; rel="dashboard"'
         *
         * @param string $linkHeaderAsString
         *
         * @return array
         */
        private function get_link_header($linkHeaderAsString)
        {
            $urls = array();

            if (preg_match_all('/<(.*?)>\s*;\s*rel="(.*?)"\s*/', $linkHeaderAsString, $matches)) {
                foreach ($matches[2] as $i => $relName) {
                    $urls[$relName] = $matches[1][$i];
                }
            }

            return $urls;
        }

        /**
         * Encode the data and attach it to the request
         *
         * @param   resource $ch   cURL session handle, used by reference
         * @param   array    $data Assoc array of data to attach
         */
        private function attach_payload(&$curlHandle, $data)
        {
            $encoded                    = json_encode($data);
            $this->last_request['body'] = $encoded;
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $encoded);
        }

        /**
         * Decode the response and format any error messages for debugging
         *
         * @param array $response The response from the curl request
         *
         * @return array|false    The JSON decoded into an array
         */
        private function decode_response($response)
        {
            $this->last_response = $response;

            if (!empty($response['body'])) {
                return json_decode($response['body'], true);
            }

            return false;
        }

        /**
         * Do post-request formatting and setting state from the response
         *
         * @param array    $response        The response from the curl request
         * @param string   $responseContent The body of the response from the curl request
         * @param resource $ch              The curl resource
         *
         * @return array    The modified response
         */
        private function validate_response($response, $responseContent, $ch)
        {
            if ($responseContent === false) {
                $this->last_error = curl_error($ch);
            } else {

                $headerSize = $response['headers']['header_size'];

                $response['httpHeaders'] = $this->convert_headers_to_array(substr($responseContent, 0, $headerSize));
                $response['body']        = substr($responseContent, $headerSize);

                if (isset($response['headers']['request_header'])) {
                    $this->last_request['headers'] = $response['headers']['request_header'];
                }
            }

            return $response;
        }

        /**
         * Check if the response was successful or a failure. If it failed, store the error.
         *
         * @param array       $response          The response from the curl request
         * @param array|false $formattedResponse The response body payload from the curl request
         *
         * @return bool     If the request was successful
         */
        private function request_succeeded($response, $formattedResponse)
        {
            $status = $this->get_http_status($response, $formattedResponse);

            if ($status >= 200 && $status <= 299) {
                $this->success = true;
                return true;
            }

            if (isset($formattedResponse['detail'])) {
                $this->errors = sprintf('%d: %s', $formattedResponse['status'], $formattedResponse['detail']);
                return false;
            }

            if ($timeout > 0 && $response['headers'] && $response['headers']['total_time'] >= $this->timeout) {
                $this->errors = sprintf('Request timed out after %f seconds.', $response['headers']['total_time']);
                return false;
            }

            $this->errors = 'Unknown error, call get_last_response() to find out what happened.';
            return false;
        }

        /**
         * Find the HTTP status code from the headers or API response body
         *
         * @param array       $response          The response from the curl request
         * @param array|false $formattedResponse The response body payload from the curl request
         *
         * @return int  HTTP status code
         */
        private function get_http_status($response, $formattedResponse)
        {
            if (!empty($response['headers']) && isset($response['headers']['http_code'])) {
                return (int)$response['headers']['http_code'];
            }

            if (!empty($response['body']) && isset($formattedResponse['status'])) {
                return (int)$formattedResponse['status'];
            }

            return 418;
        }

        public function get_last_request()
        {
            return $this->last_request;
        }

        public function get_last_response()
        {
            return $this->last_response;
        }

        public function get_last_error()
        {
            return $this->errors ? $this->errors : false;
        }

        private function send_request($command, $path, $args = []) {

            $response = $this->initialize_response($command, $path, $this->baseUrl);

            $curlHandle = curl_init();
            $this->set_headers($curlHandle,$command);

            switch ($command) {
                case MailchimpAPI::GET:
                    $query = http_build_query($args, '', '&');
                    if ($query != '') {
                        $query = '?' . $query;
                    }
                    BP_LOGGING::Instance()->log('MailchimpAPI','send_request: '.$this->baseUrl . MailchimpAPI::VERSION . $path . $query);
                    curl_setopt($curlHandle, CURLOPT_URL, $this->baseUrl . MailchimpAPI::VERSION . $path . $query);
                    break;
                case MailchimpAPI::POST:
                    curl_setopt($curlHandle, CURLOPT_URL, $this->baseUrl . MailchimpAPI::VERSION . $path);
                    curl_setopt($curlHandle, CURLOPT_POST, true);
                    $this->attach_payload($curlHandle, $args);
                    break;
                default:
                    break;
            }


            $responseContent     = curl_exec($curlHandle);
            $response['headers'] = curl_getinfo($curlHandle);
            $response            = $this->validate_response($response, $responseContent, $curlHandle);
            $formattedResponse   = $this->decode_response($response);

            curl_close($curlHandle);

            $isSuccess = $this->request_succeeded($response, $formattedResponse);

            return is_array($formattedResponse) ? $formattedResponse : $isSuccess;

        }

        public function ping() {
            $response = $this->send_request(MailchimpAPI::GET, MailchimpAPI::PING);
            BP_LOGGING::Instance()->log('MailchimpAPI','Ping response: '.var_export($response,true));
            return $response;

        }

        public function lists() {
            $response = $this->send_request(MailchimpAPI::GET, MailchimpAPI::LISTS);
            BP_LOGGING::Instance()->log('MailchimpAPI','Lists response: '.var_export($response,true));
            return $response;
        }

        public function subscribe_user($list_id, $first_name, $last_name, $email, $tag="") {
            $response = $this->send_request(MailchimpAPI::POST, sprintf(MailchimpAPI::SUBSCRIBE,$list_id),
                ['email_address' => $email,
                 'merge_fields'  => ['FNAME'=>$first_name, 'LNAME'=>$last_name],
		 'status'        => 'subscribed',
                 'tags_count'    => $tag === "" ? 0 : 1,
                 'tags'          => [$tag],
                  ]);
            BP_LOGGING::Instance()->log('MailchimpAPI','Subscribe response: '.var_export($response,true));
            return $response;
        }
    }
}

